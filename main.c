#include "stm32f10x_can.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x.h"
int uwCounter = 0;

int main(void)
{
	//SystemInit();
	//SystemCoreClockUpdate();
	CAN_PortInit();
	CAN_Configuration(CAN2, 500);
    //EXTI_ClearITPendingBit(EXTI_Line0);
     uint8_t tag[5], TxMailbox, CAN_Status;
     uint64_t rfid_tag = 0;
     CanTxMsg CanTxMessage;
     CanTxMessage.StdId = 0x321;
     CanTxMessage.ExtId = 0;
     CanTxMessage.RTR  = CAN_RTR_DATA;
     CanTxMessage.IDE = CAN_ID_STD;
     CanTxMessage.DLC = 8;
     GPIO_SetBits(GPIOB, GPIO_Pin_0);
     Delay();
     GPIO_ResetBits(GPIOB, GPIO_Pin_0);

  while (1)
  {

	 //   GPIO_SetBits(GPIOB, GPIO_Pin_0);
	 //    Delay();
	 //    GPIO_ResetBits(GPIOB, GPIO_Pin_0);
	 //    Delay();
	  TxMailbox = CAN_Transmit(CAN1, &CanTxMessage);


	while((CAN_TransmitStatus(CAN1, TxMailbox)  !=  CANTXOK) && (uwCounter  !=  0xFFFF))
	 {
		uwCounter++;
	 }
//	  TxMailbox = CAN_Transmit(CAN2, &CanTxMessage);


//	while((CAN_TransmitStatus(CAN2, TxMailbox)  !=  CANTXOK) && (uwCounter  !=  0xFFFF))
//	 {
//		uwCounter++;
//	 }
	 // uint8_t status = CAN_TransmitStatus(CAN2, TxMailbox);
  }

}

void CAN_PortInit(void)
{
GPIO_InitTypeDef GPIO_InitStructure;


RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);
RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN2, ENABLE);
RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);


GPIO_PinRemapConfig(GPIO_Remap1_CAN1, ENABLE);
/* Configure CAN pin: RX */

GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
GPIO_Init(GPIOB, &GPIO_InitStructure);

/* Configure CAN pin: TX */
GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
GPIO_Init(GPIOB, &GPIO_InitStructure);

GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
GPIO_Init(GPIOB, &GPIO_InitStructure);

/* Configure CAN pin: TX */
GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
GPIO_Init(GPIOB, &GPIO_InitStructure);

GPIO_InitTypeDef GPIO_LedInitStructure;

GPIO_LedInitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 |GPIO_Pin_2;
GPIO_LedInitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
GPIO_LedInitStructure.GPIO_Speed = GPIO_Speed_50MHz;
GPIO_Init(GPIOB, &GPIO_LedInitStructure);
}



//CAN1, CAN2 Configuration
void CAN_Configuration(CAN_TypeDef * CANx, uint8_t nCanBaudRate)
{
	RCC_ClocksTypeDef     RCC_Clocks;
CAN_InitTypeDef CAN_InitStructure;
CAN_FilterInitTypeDef CAN_FilterInitStructure;

/* CAN register init */
CAN_DeInit(CAN1);
CAN_DeInit(CAN2);
CAN_StructInit(&CAN_InitStructure);

/* CAN cell init */
CAN_InitStructure.CAN_ABOM=DISABLE;
CAN_InitStructure.CAN_AWUM=DISABLE;
CAN_InitStructure.CAN_NART=DISABLE;
CAN_InitStructure.CAN_RFLM=DISABLE;
CAN_InitStructure.CAN_Mode=CAN_Mode_Normal;

CAN_InitStructure.CAN_SJW=CAN_SJW_1tq;
CAN_InitStructure.CAN_BS1=CAN_BS1_6tq;
CAN_InitStructure.CAN_BS2=CAN_BS2_7tq;
CAN_InitStructure.CAN_Prescaler=RCC_Clocks.PCLK1_Frequency / (14 * 500000); //72MHz/2=36MHz=PCLK1 / 4 => 9000KHz / (1+10+7) => 500KHz
//CAN_InitStructure.CAN_Prescaler=29;
//CAN_Init(CAN1, &CAN_InitStructure);
//uint8_t clock_stuff = RCC_Clocks.PCLK1_Frequency;

/* CAN filter init */

CAN_FilterInit(&CAN_FilterInitStructure);

if(CAN_Init(CAN1,&CAN_InitStructure) == CANINITFAILED)
   {
	 // RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, DISABLE);
	  GPIO_SetBits(GPIOB, GPIO_Pin_2);
     return(0);
   }
if(CAN_Init(CAN2,&CAN_InitStructure) == CANINITFAILED)
   {
	 // RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, DISABLE);
	  GPIO_SetBits(GPIOB, GPIO_Pin_2);
     return(0);
   }
GPIO_SetBits(GPIOB, GPIO_Pin_1);
//CAN_ITConfig(CANx, CAN_IT_FMP0, ENABLE);

}

void Delay(void)
{
  int nTime;

  for(nTime = 0; nTime < 10000000; nTime++)
  {
  }
}
